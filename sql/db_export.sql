BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `Users` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`name`	TEXT NOT NULL,
	`alternate_name`	TEXT,
	`photo`	TEXT
);
INSERT INTO `Users` (id,name,alternate_name,photo) VALUES (1,'Test Name','Test Alternate Name','Test Photo URI');
INSERT INTO `Users` (id,name,alternate_name,photo) VALUES (2,'TN1','TAN1','TP1');
INSERT INTO `Users` (id,name,alternate_name,photo) VALUES (3,'name2',NULL,'photo2');
INSERT INTO `Users` (id,name,alternate_name,photo) VALUES (4,'a',NULL,'b');
INSERT INTO `Users` (id,name,alternate_name,photo) VALUES (5,'a',NULL,'b');
INSERT INTO `Users` (id,name,alternate_name,photo) VALUES (6,'name2',NULL,'photo2');
INSERT INTO `Users` (id,name,alternate_name,photo) VALUES (8,'name2',NULL,'photo2');
INSERT INTO `Users` (id,name,alternate_name,photo) VALUES (9,'name2',NULL,'photo2');
CREATE TABLE IF NOT EXISTS `UserTypes` (
	`user_type`	TEXT NOT NULL UNIQUE,
	PRIMARY KEY(`user_type`)
);
INSERT INTO `UserTypes` (user_type) VALUES ('Moderator');
INSERT INTO `UserTypes` (user_type) VALUES ('Contestant');
INSERT INTO `UserTypes` (user_type) VALUES ('Viewer');
INSERT INTO `UserTypes` (user_type) VALUES ('Supervisor');
CREATE TABLE IF NOT EXISTS `Teams` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`olympiad_id`	INTEGER NOT NULL,
	`country`	TEXT NOT NULL,
	FOREIGN KEY(`olympiad_id`) REFERENCES `Olympiads`(`id`),
	FOREIGN KEY(`country`) REFERENCES `Countries`(`country`)
);
INSERT INTO `Teams` (id,olympiad_id,country) VALUES (1,1,'Bulgaria');
INSERT INTO `Teams` (id,olympiad_id,country) VALUES (2,1,'Romania');
INSERT INTO `Teams` (id,olympiad_id,country) VALUES (3,1,'Greece');
CREATE TABLE IF NOT EXISTS `TeamMembers` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`user_id`	INTEGER NOT NULL,
	`team_id`	INTEGER NOT NULL,
	`user_type`	TEXT DEFAULT 'Contestant',
	FOREIGN KEY(`team_id`) REFERENCES `Teams`(`id`),
	FOREIGN KEY(`user_id`) REFERENCES `Users`(`id`)
);
INSERT INTO `TeamMembers` (id,user_id,team_id,user_type) VALUES (1,1,1,'Contestant');
INSERT INTO `TeamMembers` (id,user_id,team_id,user_type) VALUES (2,2,1,'Moderator');
INSERT INTO `TeamMembers` (id,user_id,team_id,user_type) VALUES (3,3,2,'Contestant');
INSERT INTO `TeamMembers` (id,user_id,team_id,user_type) VALUES (4,4,2,'Contestant');
INSERT INTO `TeamMembers` (id,user_id,team_id,user_type) VALUES (5,5,3,'Contestant');
INSERT INTO `TeamMembers` (id,user_id,team_id,user_type) VALUES (6,6,3,'Contestant');
CREATE TABLE IF NOT EXISTS `Tasks` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`olympiad_id`	INTEGER NOT NULL,
	`name`	TEXT NOT NULL,
	`max_score`	REAL NOT NULL DEFAULT 100.00 CHECK(max_score > 0),
	`task_number`	INTEGER NOT NULL CHECK(task_number > 0),
	`day`	INTEGER NOT NULL DEFAULT 1 CHECK(day > 0),
	FOREIGN KEY(`olympiad_id`) REFERENCES `Olympiads`(`id`)
);
CREATE TABLE IF NOT EXISTS `TaskScores` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`user_id`	INTEGER NOT NULL,
	`task_id`	INTEGER NOT NULL,
	`score`	REAL NOT NULL DEFAULT (0.0),
	FOREIGN KEY(`user_id`) REFERENCES `Users`(`id`),
	FOREIGN KEY(`task_id`) REFERENCES `Tasks`(`id`)
);
CREATE TABLE IF NOT EXISTS `Olympiads` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`year`	INTEGER NOT NULL,
	`host_city`	TEXT NOT NULL,
	`host_country`	TEXT NOT NULL,
	`start_date`	TEXT NOT NULL,
	`end_date`	TEXT NOT NULL,
	FOREIGN KEY(`host_country`) REFERENCES `Countries`(`country`)
);
INSERT INTO `Olympiads` (id,year,host_city,host_country,start_date,end_date) VALUES (1,2007,'Sofia','Bulgaria','01.07.2007','12.07.2007');
INSERT INTO `Olympiads` (id,year,host_city,host_country,start_date,end_date) VALUES (2,2008,'Bucharest','Romania','03.06.2008','15.06.2008');
CREATE TABLE IF NOT EXISTS `Countries` (
	`country`	TEXT NOT NULL UNIQUE,
	PRIMARY KEY(`country`)
);
INSERT INTO `Countries` (country) VALUES ('Bulgaria');
INSERT INTO `Countries` (country) VALUES ('Romania');
INSERT INTO `Countries` (country) VALUES ('Serbia');
INSERT INTO `Countries` (country) VALUES ('Greece');
INSERT INTO `Countries` (country) VALUES ('Albania');
COMMIT;
