select Olympiads.id, Olympiads.year, Olympiads.host_country, Olympiads.host_city, Olympiads.start_date, Olympiads.end_date, count(*) as contestants, count(distinct country) as countries
from Olympiads
	inner join Teams on Olympiads.id=Teams.olympiad_id
	inner join TeamMembers on TeamMembers.team_id=Teams.id
	inner join Users on TeamMembers.user_id=Users.id
	where member_type="Contestant"
	