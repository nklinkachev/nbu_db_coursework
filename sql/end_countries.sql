select
	distinct(Countries.country),
	count(case medal_type when "gold" then 1 else null end) as G,
	count(case medal_type when "silver" then 1 else null end) as S,
	count(case medal_type when "bronze" then 1 else null end) as B
from Countries
	inner join Teams on Countries.country=Teams.country
	inner join TeamMembers on Teams.id=TeamMembers.team_id
	inner join Medals on Medals.user_id=TeamMembers.user_id
group by Countries.country