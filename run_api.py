import sqlite3
import json
from flask import Flask, g, jsonify, request, Response


DATABASE = '/home/nklinkachev/nbu/db-praktika/db.db'

app= Flask(__name__)

#region Error Handling

class InvalidInput(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv

@app.errorhandler(InvalidInput)
def handle_invalid_input(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response
#endregion

# region DBHelpers
def make_dicts(cursor, row):
    return dict((cursor.description[idx][0], value)
                for idx, value in enumerate(row))

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE, isolation_level=None)
    db.row_factory = make_dicts
    return db

def query_db(query, args=(), one=False, rowid_out=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    result = (rv[0] if rv else None) if one else rv
    if rowid_out:
        return (cur.lastrowid, result)
    else:
        return result

def scrub(table_name):
    return ''.join( chr for chr in table_name if chr.isalnum() )

#endregion





@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()





@app.after_request
def add_cors(resp):
    """ Ensure all responses have the CORS headers. This ensures any failures are also accessible
        by the client. """
    resp.headers['Access-Control-Allow-Origin'] = request.headers.get('Origin','*')
    resp.headers['Access-Control-Allow-Credentials'] = 'true'
    resp.headers['Access-Control-Allow-Methods'] = 'POST, OPTIONS, GET'
    resp.headers['Access-Control-Allow-Headers'] = request.headers.get( 
        'Access-Control-Request-Headers', 'Authorization' )
    # set low for debugging
    if app.debug:
        resp.headers['Access-Control-Max-Age'] = '1'
    return resp







@app.route('/favicon.ico')
def favicon():
    return ""

@app.route('/<string:table>', methods=['GET'])
def get_all(table):
    try:
        results = query_db('SELECT * FROM {}'.format(scrub(table)))
    except sqlite3.OperationalError as e:
        raise InvalidInput(e.args, status_code=404)
    return Response(json.dumps(results), mimetype='application/json')

@app.route('/<string:table>/<string:id>', methods=['GET'])
def get_id(table, id):
    try:
        result = query_db('SELECT * FROM {} WHERE id=?'.format(scrub(table)), scrub(id), one=True)
    except sqlite3.OperationalError as e:
        raise InvalidInput(e.args, status_code=404)
    if (len(result)==0):
        raise InvalidInput('There is no object of type "{}" and id "{}"'.format(table, id))
    return Response(json.dumps(result), mimetype='application/json')

@app.route('/<string:table>', methods=['POST'])
def create_object(table):
    param_names=[]
    param_values=[]
    for key in request.form.keys():
        param_names.append(scrub(key))
        param_values.append(request.form[key])
    param_values_query_part = ','.join('?'*len(param_names))


    query = 'INSERT INTO {} ({}) VALUES ({})'.format(scrub(table), ','.join(param_names), param_values_query_part)

    try:
        rowid, _ = query_db(query, param_values, one=True, rowid_out=True)
    except sqlite3.OperationalError as e:
        raise InvalidInput(e.args, status_code=400)
    
    result_object = query_db('SELECT * FROM {} WHERE id={}'.format(scrub(table), rowid), one=True)
    return Response(json.dumps(result_object), mimetype='application/json')

@app.route('/<string:table>/<string:id>', methods=['PUT'])
def update_object(table,id):
    key_value_pairs = []
    param_values = []
    for key in request.form.keys():
        key_value_pairs.append('{}=?'.format(scrub(key)))
        param_values.append(request.form[key])
    key_value_query_part = ','.join(key_value_pairs)

    query = 'UPDATE {} SET {} WHERE id={}'.format(scrub(table), key_value_query_part, scrub(id))

    try:
        query_db(query, param_values)
    except sqlite3.OperationalError as e:
        raise InvalidInput(e.args, status_code=400)
    
    result_object = query_db('SELECT * FROM {} WHERE id={}'.format(scrub(table), scrub(id)), one=True)
    return Response(json.dumps(result_object), mimetype='application/json')

@app.route('/<string:table>/<string:id>', methods=['DELETE'])
def delete_object(table,id):
    try:
        result = query_db('DELETE FROM {} WHERE id={}'.format(scrub(table), scrub(id)), one=True) 
    except sqlite3.OperationalError as e:
        raise InvalidInput(e.args, status_code=404)
    return Response("", mimetype='application/json')

@app.route('/ui/<string:table>')
def serve_table(table):
    with open('ui/table_view.html', 'r') as myfile:
        data=myfile.read()
        return data








@app.route('/ui/d3/d3.min.js')
def serve_d3():
    with open('ui/d3/d3.min.js', 'r') as myfile:
        data=myfile.read()
        return data

if __name__ == '__main__':
    app.run(debug=True)
